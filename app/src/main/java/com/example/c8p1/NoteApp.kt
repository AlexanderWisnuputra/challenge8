package com.example.c8p1

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class c8p1 : Application()