package com.example.c8p1.features.note.domain.use_case

import com.example.c8p1.features.note.domain.model.Note
import com.example.c8p1.features.note.domain.repository.NoteRepository

class DeleteNote(
    private val repository: NoteRepository
) {

    suspend operator fun invoke(note: Note) {
        repository.deleteNote(note)
    }
}