package com.example.c8p1.features.note.domain.use_case

import com.example.c8p1.features.note.domain.model.Note
import com.example.c8p1.features.note.domain.repository.NoteRepository

class GetNoteById(
    private val repository: NoteRepository
) {

    suspend operator fun invoke(id: Int): Note? {
        return repository.getNote(id)
    }
}